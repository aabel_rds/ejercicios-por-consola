                            // EJERCICIO 26

function multiple(valor, multiple){
    
    var total = valor % multiple;//        Esta funcion calcula los multiploss de cualquier numero.

    if(total==0){
        return true;
    }
    else{

        return false;
    }
}

var mult_3 = [];
var mult_5 = [];

for(var i = 1 ; i <= 1000; i++ ){        // Creamos un bucle del 1 al 1000 y aplicamos la función creada multiple
                                        //  le pasamos los parametros y lo pusheamos en su array.
    if (multiple(i,3)){

        mult_3.push(i);
    }

    if (multiple(i,5)){

        mult_5.push(i);
        
    }
      

}
var totalMult_3 = 0;
var totalMult_5 = 0;

for( var i=0 ; i < mult_3.length ; i++ ){       // Recorremos el array creado y junto al operador =+ 
                                                // sumamos el array, el operador recorre el array situandose en 0
    totalMult_3 += mult_3[i];                   // lo almacena en la var y luego le suma el siguiente hasta cumplir el bucle

}

console.log(totalMult_3);


for(var i=0 ; i < mult_5.length ; i++){

    totalMult_5 += mult_5[i];
}

console.log(totalMult_5);

console.log(totalMult_5 + totalMult_3);

document.getElementById('ej-1').innerHTML= "Ejercicio Nº 26";
document.getElementById('m3').innerHTML= totalMult_3;
document.getElementById('m5').innerHTML= totalMult_5;
document.getElementById('total').innerHTML= ( totalMult_3 + totalMult_5);


                            // Ejercicio 27


var numero1=0
var numero2=1
var fibon = [numero1,numero2];

for (var i=0; i <= 1000000 ; i++){         // creamos un bucle que ejecute new_num y que lo guarde en el array fibon
                                           // al guardarlo cambiará la información de número 1 y número 2
    var new_num = numero1 + numero2;      // siguiendo este bucle creamos el fibonacci
    fibon.push(new_num);
    numero1=numero2;
    numero2=new_num;

}
console.log(fibon);

function pares(numero){     // creamos una función el cual calcule los números pares

    var par = numero % 2;

    if(par==0){

        return true;
    }
    else {
        return false;
    }

}
var fibonPar=[];

for (var x of fibon){   // Recorremos el array fibon que contiene el fibonacci creado pasandole la condición 
                        // if junto a la función creada, el resultado lo guardamos en el array vacio "fibonPar"
    if(pares(x)){       // alamacenando solo los datos pares del fibonacci
        
        fibonPar.push(x);
    }
    
}
console.log(fibonPar);

var totalPar=0;
var totalej=[];
for(x of fibonPar){             // recorremos el nuevo array (fibonPar) diciendole que por cada resultado lo almacene
    totalPar += x;              // y le sume el siguiente valor.
    while(totalPar<=1000000){   // todo esto lo hará mientras que totalPar sea menor o igual  a 1M
                                
        totalej.push(totalPar); // cada resultado lo guardamos en un nuevo array.
    
        break;
        
    }
    
}

document.getElementById('ej-2').innerHTML= "Ejercicio Nº 27";
document.getElementById("fibo-pares").innerHTML=totalej[totalej.length - 1];


//                          Ejercicio 28

function mult7(numero){

    var totalmult7 = numero % 7;

    if(totalmult7==0){

        return true;
    }
    else{
        return false;
    }
}

var nºDiv1000 = [];

for(var i = 0; i < 20000; i++){

    if( mult7(i) ){

        while(i<1000){

            nºDiv1000.push(i);

            break;
        }

    }
}
console.log(nºDiv1000);

totalej28=[];

function factorial(numero){
    if(numero <= 1){
        return 1;                   // función recursiva 
    }
    else{
        return numero * factorial(numero-1);
    }
}

t=0;

for(x of nºDiv1000){
    
    factorial(x);

    if(factorial(x)){
        
        totalej28.push( t += (factorial(x)) );
    }

}
document.getElementById("ej-3").innerHTML="Ejercicio 28";
document.getElementById("factorial").innerHTML = totalej28[totalej28.length-1];

//                              Ejercicio 29 
var tj29=0;


for(var i = 0; i<10000; i+=2 ){

    tj29 += i;

}
console.log(tj29);

function totalej29(numero){

    if(numero % 44 ==0){

        return "Es divisible entre 44";
    }
    else{

        return "No es divisible entre 44";
    }
}
document.getElementById("ej-4").innerHTML="Ejercicio 29";
document.getElementById("parEntre4").innerHTML = (totalej29(tj29));

//                                      Ejercicio 30

function mult3(numero){

    var m3 = numero % 3;
    if(m3==0){

        return true;
    }
    else{

        return false;
    }
}
p = 1;
producto = [];

for(i=1; i<100 ; i++){

    if(mult3(i)){
        
        p *= i;
    }

}
function div7(numero){
    
    if(numero % 7 ==0 ){

        return "Es divisible entre 7";

    }else{

        return "No es divisible entre 7"
    }
}

console.log(p);
document.getElementById("ej-5").innerHTML = "Ejercicio 30";
document.getElementById("eje-5").innerHTML = (div7(p));


//                      Ejercicio 31  Nº primos

document.getElementById("ej-6").innerHTML = "Ejercicio 31";
var numero = parseInt(prompt("INTRODUCE UN NUMERO MENOR QUE 100"));

var cantidadNumerosDivisiblesParaUnPrimo =2;
var cantidadDivisible = 0;

function nprimo(num){
    for (i = 1; i<=num ; i++){

        if(num % i == 0){

            cantidadDivisible++;
        }
    }

    if(cantidadDivisible == cantidadNumerosDivisiblesParaUnPrimo){

        document.getElementById("eje-6").innerHTML = "El numero "+ numero +" "+ "Es primo";
    }
    else{

        document.getElementById("eje-6").innerHTML = "El numero " + numero +" "+ "No es primo"
    }
}
nprimo(numero);