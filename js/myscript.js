                        //  Ejercicio 1
function ejercicio01() {
    function area(lado) {

        return lado ** 2;

    }
    function Perimetro(lado){
        return lado * 4;
    }

    var lado = 2;
    var ll = area(lado);
    var p = Perimetro(lado);
    var uno = "Ejercicio 1";
    console.log(uno);
    console.log(ll);
    console.log(p);
}

ejercicio01();
//                          EJERCICIO 2
function ejercicio02(){
    function area(b,h){
        return (b * h) / 2;
    }
    function Perimetro(a,b,c){
        return (a+b+c);
    }
    var b = 25;
    var h = 15;
    var a = 20;
    var c = 12;
    var triangulo = area(b,h);
    var Per = Perimetro(a,b,c);
    var dos = "Ejercicio 2";
    console.log(dos);
    console.log(triangulo);
    console.log(Per);
    
}
ejercicio02();

//                      EJERCICIO 3

function ejercicio03(){

    function area(b,a){

        return b * a;
    }
    function Perimetro(b,a){

        return 2 * (b + a);
    }
    var a = 4;
    var b = 8;
    var _area = area(b,a);
    var Per = Perimetro(b,a);
    var tres = "Ejercicio 3"
    console.log(tres);
    console.log(_area);
    console.log(Per);
}
ejercicio03();

//                      EJERCICIO 4

function ejercicio04(){

    function area(b,h){

        return b * h;
    }
    function Perimetro(b,a){

        return 2 * (b * a);
    }
    var a = 4;
    var b = 8;
    var h = 3;
    var _area = area(b,h);
    var Per = Perimetro(b,a);
    var cuatro = "Ejercicio 4"
    console.log(cuatro);
    console.log(_area);
    console.log(Per);
}
ejercicio04();

//                      EJERCICIO 5 

function ejercicio05(){

    function area(D,d){

        return (D * d)/2;
    }
    function Perimetro(a){

        return 4 * (a);
    }
    var a = 5;
    var D = 8;
    var d = 4;
    var _area = area(D,d);
    var Per = Perimetro(a);
    var cinco = "Ejercicio 5"
    console.log(cinco);
    console.log(_area);
    console.log(Per);
}
ejercicio05();

//                      Ejercicio 6 

function ejercicio06(){

    function area(D,d){

        return (D * d)/2;
    }
    function Perimetro(a,b){

        return 2 * (a + b);
    }
    var a = 4;
    var D = 7;
    var d = 2;
    var b = 9;
    var _area = area(D,d);
    var Per = Perimetro(a,b);
    var seis = "Ejercicio 6"
    console.log(seis);
    console.log(_area);
    console.log(Per);
}
ejercicio06();

//                            EJERCICIO 7

function ejercicio07(){

    function area(B,b,h){

        return ((B + b) * h) / 2;

    }
    function Perimetro(B,b,a,c){

        return (B+b+a+c);
    }
    var B = 7;
    var b = 4;
    var c = 5;
    var h = 5;
    var a = 4;
    var siete = "Ejercicio 7";
    var _area = area(B,b,h);
    var Per = Perimetro(B,b,a,c)
    console.log(siete);
    console.log(_area);
    console.log(Per);
    
}
ejercicio07();

//                  Ejercicio 8 

function ejercicio08(){

    function Perimetro(n,b){

        return n * b;
    }

    function Area(P,a){
        
        return (P * a) / 2;
    }

var n = 6;
var b = 2;
var a = 3;
var P = Perimetro(n,b);
var ocho = "Ejercicio 8";
var Per = Perimetro(n,b);
var _area = Area(P,a);
console.log(ocho);
console.log(Per);
console.log(_area);

}
ejercicio08();

//              Ejercicio 9 

function ejercicio09(){

    function Area(r){
        
        return Math.PI * (r**2);
    }

    function Perimetro(r){

        return 2 * Math.PI * r;
    }

    var r = 3;
    var nueve = "Ejercicio 9";
    var _area = Area(r).toFixed(3);
    var Per = Perimetro(r).toFixed(3);
    console.log(nueve);
    console.log(_area);
    console.log(Per);

}
ejercicio09();

//              Ejercicio 10 

function ejercicio10(){

    function Area(R,r){

        return Math.PI * ((R**2)-(r**2));
    }

    var R = 5;
    var r = 3.5;
    var diez = "Ejercicio 10";
    var _area = Area(R,r).toFixed(3);
    console.log(diez);
    console.log(_area);
}
ejercicio10();

//              Ejercicio 11 

function ejercicio11(){

    function Area(R,n){

        return (Math.PI * (R**2) * n)/360;
    }

    var R = 10;
    var n = 75;
    var once = "Ejercicio 11";
    var _area = Area(R,n).toFixed(3);
    console.log(once);
    console.log(_area);
}
ejercicio11();

//      Ejercicio 12

function ejercicio12(){

    function Area(R,h){

        return ((2 * Math.PI)*R)*(h+R);
    }

    function volumen(R,h){

        return Math.PI*(R**2)*h;
    }
    var R = 2;
    var h = 10;
    var doce = "Ejercicio 12";
    var _area = Area(R,h).toFixed(3);
    var vol = volumen(R,h).toFixed(3);
    console.log(doce);
    console.log(_area);
    console.log(vol);
}
ejercicio12();

//          Ejercicio 13

function ejercicio13(){

    function Area(R,g){

        return (Math.PI*R)*(R + g);

    }

    function volumen(R,h){

        return ( Math.PI * (R**2) * h ) / 3;
    }

    var R = 3;
    var g = 9;
    var h = 7;
    var _area = Area(R,g).toFixed(3);
    var vol = volumen(R,h).toFixed(3);
    var trece = "Ejercicio 13";
    console.log(trece);
    console.log(_area);
    console.log(vol);
}
ejercicio13();

//              Ejercicio 14

function ejercicio14(){

    function Area(g,r,R){

        return Math.PI * ( (g * (r+R)) + (r**2) + (R**2) );
    }

    function volumen(h,R,r){
        
        return ( ( Math.PI * h ) *( (R**2) + (r**2) + (R*r))) / 3;

    }

    var g = 9;
    var R = 3;
    var h = 7;
    var r = 1.5;
    var catorce = "Ejercicio 14";
    var _area = Area(g,r,R).toFixed(3);
    var vol = volumen(h,R,r).toFixed(3);
    console.log(catorce);
    console.log(_area);
    console.log(vol);

}
ejercicio14();

//                  Ejercicio 15

function ejercicio15(){

    function Area(R){

        return (4 * (Math.PI * (R**2)));
    }

    function volumen(R){

        return (4 * Math.PI * (R**3)) / 3;

    }

    var R = 5;
    var quince = "Ejercicio 15";
    var _area = Area(R).toFixed(3);
    var vol = volumen(R).toFixed(3);
    
    console.log(quince);
    console.log(_area);
    console.log(vol);

}

ejercicio15();


//                          Ejercicio 16

function ejercicio16(){

    function Area(R,h){

        return (2 * Math.PI * R * h);
    }
    
    function volumen(h,R){

        return ((Math.PI*(h**2))*((3*R)-h)) / 3;

    }
    var h = 2.5;
    var R = 5;
    var dieciseis = "Ejercicio 16";
    var _area = Area(R,h).toFixed(3);
    var vol = volumen(h,R).toFixed(3);
    console.log(dieciseis);
    console.log(_area);
    console.log(vol);
}
ejercicio16();

//                              Ejercicio 17

function ejercicio17(){

    function Area(R,n){

        return ( 4 * Math.PI * (R**2) * n ) / 360
    }

    function volumen(R,n){

        return ( 4 * Math.PI * (R**3) * n ) / (3* 360);
    }
    var R = 5;
    var n = 90;
    var diecisiete = "Ejercicio 17";
    var _area = Area(R,n).toFixed(3);
    var vol = volumen(R,n).toFixed(3);
    console.log(diecisiete);
    console.log(_area);
    console.log(vol);
}
ejercicio17();

//                                  Ejercicio 18 

function ejercicio18(){

    function Area(R,h){

        return (2*Math.PI) * R * h;
    }

    function volumen(h,r,rp){

        return ((Math.PI * h) * ( (h**2) + (3*(r**2)) + (3*(rp**2)))) / 6;
    }
    var R = 5;
    var r = 2.5;
    var rp = 2;
    var h = 1;
    var dieciocho = "Ejercicio 18";
    var _area = Area(R,h).toFixed(3);
    var vol = volumen(h,r,rp).toFixed(3);
    console.log(dieciocho);
    console.log(_area);
    console.log(vol);
}
ejercicio18();

//                              Ejercicip 19
var lado = 4;
function ejercicio19(lado){

    function Area(){

        return 6 * (lado**2);
    }

    function volumen(){

        return lado**3;
    }
    
    var diecinueve = "Ejercicio 19";
    var _area = Area().toFixed(3);
    var vol = volumen().toFixed(3);
    console.log(diecinueve);
    console.log(_area);
    console.log(vol);
}
ejercicio19(lado);

//                                  Ejercicio 20 

function ejercicio20(){

    function Area(a,b,c){

        return (2 * ((a * b) + (a * c) + (b * c)));
    }

    function volumen(a,b){

        return a * b * c;
    }

    var a = 6;
    var b = 2;
    var c = 4;
    var veinte = "Ejercicio 20";
    var _area = Area(a,b,c);
    var vol = volumen(a,b);
    console.log(veinte);
    console.log(_area);
    console.log(vol);
}
ejercicio20();
